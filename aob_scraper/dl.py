from dataclasses import dataclass
from threading   import Lock
from typing	 import Any, Optional
from requests	 import HTTPError, Response

from aob_scraper.formats import COMMON_FORMATS, get_mapping
from yt_dlp.YoutubeDL import YoutubeDL
from .utils import Error, FileFormat, Ok, Result, State, TriState, audio_cmdline, download_parser, env_args, first, is_empty, log, system2, not_empty, file_exists_anyf, video_cmdline

import os, requests, bs4, tempfile, shutil, re, subprocess, signal, argparse

# Mapping the user-inputted classifiers to the metadata field on AoB
classifier_dict: dict[str, str] = {
    "genre"     : "Genre",
    "instrument": "Instrument",
    "series"    : "Serie",
    "city"      : "City",
    "year"      : "Year",
}

# Mapping the genre on AoB to genre metadata
genre_mapping: dict[str, str] = {
    "latin church music"    : "Latin Church Music",
    "cantatas"              : "Cantata",
    "chamber music"         : "104",
    "chorales"              : "Chorale",
    "harpsichord works"     : "33", # Fallback to "Instrumental"
    "motets"                : "Motet",
    "oratorios and passions": "Oratorio/Passion",
    "orchestral works"      : "Orchestral",
    "organ works"           : "33", # Fallback to "Instrumental"
    "songs and arias"       : "28", # Fallback to "Vocal"
    "_fallback"             : "150", # "Baroque"
}

@dataclass
class Movement:
    name: str
    start: float
    end: float

@dataclass
class RetreivedData:
    """Stores all data scraped from the HTML page."""

    # ID of the YouTube video to download
    yt_id: str

    # Metadata
    title: str
    album: str
    genre: str
    year: int
    performer: str
    conductor: str
    track: int

    movements: list[Movement]

    # Text metadata
    orig_lyrics: str # Original lyrics (usually German/Latin/Italian)
    trans_lyrics: str # Translated to English

    def check_props(self, _print: bool) -> TriState:
        """Returns TRUE if all the properties are full, NEUTRAL if all of them are full except for lyrics, and FALSE if more than one non-lyrics property is None.
        Prints the results if _print is True."""

        logstr = "aobDl: RetreivedData: check_props:"

        if not_empty(self.yt_id):
            if _print: log(logstr, f"Video ID is {self.yt_id}")
        else:
            if _print: log(logstr, "Video ID is empty or None!", True)
            return TriState.FALSE
        if not_empty(self.title):
            if _print: log(logstr, f"Title is {self.title}")
        else:
            if _print: log(logstr, "Title is empty or None!", True)
            return TriState.FALSE
        if not_empty(self.album):
            if _print: log(logstr, f"Album is {self.album}")
        else:
            if _print: log(logstr, "Album is empty")
        if self.orig_lyrics is None or is_empty(self.orig_lyrics):
            return TriState.NEUTRAL
        if self.trans_lyrics is None or is_empty(self.trans_lyrics):
            return TriState.NEUTRAL

        return TriState.TRUE


def for_ffmetadata(input: str) -> str:
    """Escape characters for the ffmetadata format."""

    return input.replace(
        # \ -> \\
        "\\", "\\\\"
    ).replace(
        # # -> \#
        "#", "\\#"
    ).replace(
        # newline -> \n
        "\n", "\\\n"
    ).replace(
        # = -> \=
        "=", "\\="
    ).replace(";", "\\;") # ; -? \;

def do_work(video_id: str, title: str,
            album: Optional[str], genre: str, year: int, performer: str,
            conductor: str, track: int, movements: list[Movement], lyrics_orig: str,
            lyrics_trans: str, s: State,
            dir_list: Optional[list[str]] = None, lock: Optional[Lock] = None):
    """Download the video, convert, cut, and apply metadata."""

    log("aobDl: do_work:",
"""Video ID: %s
Title: %s
BWV Number: %i
%s
Genre ID: %s
%s
%s
%s
Lyrics (Original): %s
Lyrics (Translated): %s
Movements:
%s
""" % (
    video_id,
    title,
    s.bwv,
    "Album: " + str(album) if album != None and not_empty(album) else "",
    genre,
    "Year: " + str(year) if year != 0 else "",
    "Performer: " + performer if not_empty(performer) else "",
    "Track: " + str(track) if track != 0 else "",
    lyrics_orig,
    lyrics_trans,
    "\n\n".join(
        f"Name: {m.name}\nStart: {m.start}\nEnd: {m.end}" for m in movements
    )
))

    # A temporary directory for us to work with
    temp_dir = tempfile.mkdtemp()


    # Compatibility with aobAll
    if dir_list is None:
        # Clean up temdir if interrupted (e.g. by C-c). We want to set this up ASAP, so
        # I do it as soon as the directory is created.
        def interrupt():
            print() # In case some other stuff was printed, e.g. YTDL
            log("aobDl: do_work: interrupt:", "Interrupt called, deleting tempdir...")
            shutil.rmtree(temp_dir)
            log("aobDl: do_work: interrupt:", "Terminating peacefully.")
            exit(0)

        signal.signal(signal.SIGINT, lambda _, __: interrupt())
    elif lock is not None:
        with lock: dir_list.append(temp_dir)

    metadata = os.path.join(temp_dir, "metadata.ffmetadata")

    # How much to cut from the start?
    if s.cut_seconds >= 0: cut: float = s.cut_seconds
    elif not_empty(movements): cut: float = movements[0].start
    else: cut: float = 7

    # YoutubeDL instance
    if not s.only_add_meta:
        ytdl_opts: dict[str, Any] = {
            "outtmpl": os.path.join(temp_dir, "%(id)s.%(ext)s"),
            "format": "bestvideo+bestaudio/best" if s.should_use_video() else "bestaudio/best",
        }

        # Download the video
        with YoutubeDL(ytdl_opts) as ytdl:
            downloaded: str = ytdl.prepare_filename(ytdl.extract_info(f"https://youtube.com/watch?v={video_id}", download=True))

        # It looks like we only need this intermediate step when raising pitch
        temp0 = os.path.join(temp_dir, "temp0." + s.ext())
        if s.should_raise():
            # Cut off the intro
            system2("aobDl: do_work:", f"ffmpeg -i {downloaded} -ss {cut} -c copy {temp0}")

    lyrics_text = f"""=============
= Original =
=============

{lyrics_orig}


==============
= Translated =
==============

{lyrics_trans}
""" if not_empty(lyrics_orig) and not_empty(lyrics_trans) else ""

    # Construct new metadata with the needed options + movements
    with open(metadata, "w") as file:
        lines: list[str] = [
            ";FFMETADATA1\n",
            "title=%s\n" % for_ffmetadata(title),
            "artist=Netherlands Bach Society\n",
            "composer=Johann Sebastian Bach\n",
            "genre=%s\n" % for_ffmetadata(genre),
            "comment=Recorded by the Netherlands Bach Society for Project All of Bach.\\\n",
            "Netherlands Bach Society Page: https://www.bachvereniging.nl/en/bwv/bwv-%s\\\n" % for_ffmetadata(f"{s.bwv}{s.modifier}"),
            "Downloaded Video: https://youtu.be/%s\n" % for_ffmetadata(video_id),
            "lyrics=%s\n" % for_ffmetadata(lyrics_text),
        ]
        if album != None and not_empty(album): lines.append("album=%s\n" % for_ffmetadata(album))
        if year != 0: lines.append("date=%i\n" % year)
        if track != 0: lines.append("track=%i\n" % track)

        if not_empty(performer):
            lines.append("artist=%s\n" % performer)
            lines.append("performer=%s\n" % performer)
        else: lines.append("performer=Netherlands Bach Society\n")
        if not_empty(conductor):
            lines.append("conductor=%s\n" % conductor)
            if is_empty(performer):
                lines.append("artist=%s\n" % conductor)
        if is_empty(performer) and is_empty(conductor):
            lines.append("artist=Netherlands Bach Society\n")

        chapter_subt = cut if s.only_add_meta or s.should_raise() else 0

        if not_empty(movements):
            if s.should_raise():
                assert "temp0" in locals(), "Something very wrong happened here (temp0)"
            elif not s.only_add_meta:
                assert "downloaded" in locals(), "Something very wrong happened here (downloaded)"

            len_file = s.file_name() if s.only_add_meta else (temp0 if s.should_raise() else downloaded)
            # The NBS didn't bother to make the ending of the last movement
            # be the actual ending, and made it an hour or smth.
            tgtLength = float(subprocess.check_output([
                "ffprobe",
                "-v", "error",
                "-show_entries", "format=duration",
                "-of", "default=noprint_wrappers=1:nokey=1",
                len_file
            ]))
            movements[-1].end = min(movements[-1].end, tgtLength)

        # Convert movements to ffmetadata format
        for movement in movements:
            lines += [
                "\n",
                "[CHAPTER]\n",
                "TIMEBASE=1/1000\n",
                "START=%s\n" % ((movement.start - chapter_subt) * 1000.0),
                "END=%s\n" % ((movement.end - chapter_subt) * 1000.0),
                "title=%s\n" % for_ffmetadata(movement.name),
            ]

        file.writelines(lines)

    if not s.only_add_meta:
        if s.should_raise():
            # Yeah, that's what I get for using Python
            assert "temp0" in locals(), "Something very wrong happened here (temp0)"

            # The audio file that rubberband will manipulate
            temp1 = os.path.join(temp_dir, "temp1.wav")
            # Rubberband's output
            temp2 = os.path.join(temp_dir, "temp2.wav")

            # Extract (or convert) audio
            system2("aobDl: do_work:", f"ffmpeg -i {temp0} {temp1}")
            # Raise the pitch!
            system2("aobDl: do_work:", f"rubberband-r3 --formant -p {s.raise_by} {temp1} {temp2}")

            audio = audio_cmdline(s, "wav")

            if s.should_use_video():
                existing_vcodec: str = subprocess.check_output([
                    "ffprobe",
                    "-v", "error",
                    "-select_streams", "v:0",
                    "-show_entries", "stream=codec_name",
                    "-of", "default=nw=1:nk=1",
                    temp0
                ]).decode("utf-8").strip()
                video = video_cmdline(s, existing_vcodec)

                # Use three inputs, taking the video from the first,
                # audio from the second, metadata from the third and join 'em all!
                system2(
                    "aobDl: do_work:",
                    "ffmpeg -y -i '%s' -i '%s' -i '%s' %s %s -map 0:v:0 -map 1:a:0 -map_metadata 2 '%s'" % (
                        temp0, temp2, metadata,
                        video, audio,
                        s.file_name()
                    ),
                )
            else:
                # Use two inputs, taking the audio from the first,
                # metadata from the second and join 'em all!
                system2("aobDl: do_work:", "ffmpeg -y -i '%s' -i '%s' %s -vn -map_metadata 1 '%s'" % (temp2, metadata, audio_cmdline, s.file_name()))
        else:
            assert "downloaded" in locals(), "Something very wrong happened here (downloaded)"

            existing_acodec: str = subprocess.check_output([
                "ffprobe",
                "-v", "error",
                "-select_streams", "a:0",
                "-show_entries", "stream=codec_name",
                "-of", "default=nw=1:nk=1",
                downloaded
            ]).decode("utf-8").strip()
            audio = audio_cmdline(s, existing_acodec)

            existing_vcodec: str = subprocess.check_output([
                     "ffprobe",
                     "-v", "error",
                     "-select_streams", "v:0",
                     "-show_entries", "stream=codec_name",
                     "-of", "default=nw=1:nk=1",
                     downloaded
            ]).decode("utf-8").strip() if s.should_use_video() else ""
            video = video_cmdline(s, existing_vcodec)

            # Use two inputs, taking the audio(+video) from the first,
            # metadata from the second and join them all!
            system2(
                "aobDl: do_work:",
                "ffmpeg -y -i '%s' -i '%s' -map_metadata 1 -ss '%s' %s %s '%s'" % (
                    downloaded, metadata,
                    cut,
                    audio, video,
                    s.file_name()
                )
            )
    else:
        output = os.path.join(temp_dir, "file." + s.ext())
        shutil.move(s.file_name(), output)
        system2(
            "aobDl: do_work:",
            "ffmpeg -i '%s' -i '%s' -map_metadata 1 -c copy '%s'" % (output, metadata, s.file_name())
        )

    # Clean up
    shutil.rmtree(temp_dir)
    if dir_list is not None and lock is not None:
        with lock: dir_list.remove(temp_dir)

def parse_html(url: str, classifiers: list[str]) -> Result[RetreivedData, Response]:
    """Scrape data from AoB page."""

    # Fetch the URL
    resp = requests.get(url)
    if resp.status_code < 200 or resp.status_code >= 400:
        return Error(resp)

    # Command-line classifier option can be the first letter only
    for index in range(len(classifiers)):
        if classifiers[index] not in classifier_dict:
            for key in classifier_dict.keys():
                if classifiers[index] == key[0]: classifiers[index] = key

    soup: Any = bs4.BeautifulSoup(resp.content, "html.parser")
    title = str(soup.h1.string)
    playerdiv = soup.find(id="player", name="div")
    if playerdiv is None: return Error(resp)
    yt_id = str(soup.find(id="player", name="div")["data-youtube-id"])

    _tempmatch = re.search(r'no\. ?(\d+)', title, re.I)
    if _tempmatch: track: int = int(_tempmatch.group(1))
    else: track: int = 0

    # Classifier fields
    dl_children: list[bs4.element.Tag] = list(filter(
           lambda x: isinstance(x, bs4.element.Tag),
           soup.dl.children
    ))
    fields: dict[str, str] = {}
    for i in range(0, len(dl_children), 2):
           dl_children[i].find_next_sibling
           child = dl_children[i].string
           child2 = dl_children[i+1].string
           if child != None and child2 != None:
               fields[child] = child2

    genre = genre_mapping[
           fields[classifier_dict["genre"]].lower()
    ] if fields[classifier_dict["genre"]].lower() in genre_mapping else genre_mapping["_fallback"]

    year: str = ""
    if classifier_dict["year"] in fields:
        _tempmatch = re.search(r'\d+', fields[classifier_dict["year"]])
        if _tempmatch: year: str = _tempmatch.group()
    yearint: int = int(year) if not_empty(year) else 0

    h2s = "\n".join(map(
        lambda x: x.text,
        soup.find_all(name="h2")
    ))
    _tempmatch = re.search(r'performed by ([\w \t]+) ?\r?\n', h2s, re.I)
    if _tempmatch: performer: str = _tempmatch.group(1).strip().removeprefix("the")
    else: performer: str = ""

    _tempmatch = re.search(r'conducted by ([\w \t]+) ?\r?\n', h2s, re.I)
    if _tempmatch: conductor: str = _tempmatch.group(1).strip().removeprefix("the")
    else: conductor: str = ""

    album: str = ""
    for classifier in classifiers:
        if classifier_dict[classifier] not in fields: continue
        _temp_classifier = fields[classifier_dict[classifier]]
        if classifier == "year": _temp_classifier = year
        album = _temp_classifier.title() if classifier == "genre" else _temp_classifier
        break

    # It'd be too long if I did everything in one assignment
    # Filtering out the "Menu" option
    _movement_ls_f = filter(
           lambda x: "data-seekto-start" in x.attrs and not re.search(r'\bmenu\b', x.string, re.I),
           soup.find_all(name="li")
    )

    # Strip whitespace, remove leading digit, obtain time information
    movements = list(map(
           lambda x: Movement(re.sub(r'^\d+\. ', "", x.string.strip(), flags=re.S),
                              float(x["data-seekto-start"]),
                              float(x["data-seekto-end"])),
           _movement_ls_f
    ))

    lyrics_h2 = first(lambda x: x.string != None and x.string.lower() in ["vocal texs", "vocal texts"], soup.find_all(name="h2"))
    orig_content: str = ""
    trans_content: str = ""
    if lyrics_h2 != None:
           div = lyrics_h2.find_next_sibling(name="div")
           # I'll just... not touch this
           for child in div.find_all(name="div"):
               translation: bool
               if None in [child.h3, child.div]: continue

               translation = child.h3.string.lower() != "original"
               for ptag in child.div.find_all(name="p"):
                   str_to_add = "\n\n"
                   for childd in ptag.children:
                       if isinstance(childd, bs4.element.Tag): str_to_add += "\n"
                       elif isinstance(childd, bs4.element.NavigableString): str_to_add += childd.strip()
                   if translation: trans_content += str_to_add
                   else: orig_content += str_to_add
    return Ok(RetreivedData(yt_id, title, album, genre, yearint, performer, conductor, track, movements, orig_content, trans_content))

def download(argv: list[str], dir_list: Optional[list[str]] = None, lock: Optional[Lock] = None, unstrict: bool = False):
    parser = argparse.ArgumentParser(prog=argv[0], description="Download an AoB performance", parents=[download_parser()])
    parser.add_argument("bwv", type=int, help="The BWV catalogue number", metavar="NUMBER")
    modifier_g = parser.add_mutually_exclusive_group()
    modifier_g.add_argument("--modifier", help="A little bit that comes after the number", default="")
    modifier_g.add_argument("--dash", help="A modifier preceded by a dash", default="")
    args = parser.parse_args([*env_args(), *argv[1:]])

    bwv: int = args.bwv
    classifiers: list[str] = args.classifiers
    dash: str = args.dash
    modifier: str = ("-" + dash) if not_empty(dash) else args.modifier
    f_ext: str = args.extension
    acodec: str = args.acodec
    vcodec: str = args.vcodec
    cut_seconds: float = args.cut
    only_add_meta: bool = args.no_download
    force_download: bool = args.force
    raise_by: float = args.raises

    f_format: FileFormat
    if acodec == "_default":
        _format = first(lambda f: f.extension == f_ext, COMMON_FORMATS)
        if _format: f_format = _format
        else: f_format = FileFormat(f_ext, "", "")
    elif vcodec == "_default":
        f_format = FileFormat(f_ext, acodec)
    else:
        f_format = FileFormat(f_ext, acodec, vcodec)
    s = State(bwv, classifiers, modifier, f_format, raise_by, only_add_meta, cut_seconds)

    log("aobDl:", f"Proceeding to try and download {s}")

    # unstrict -- don't ignore files of different format
    fname = file_exists_anyf(s)
    cond = fname if unstrict else os.path.isfile(s.file_name())

    if not only_add_meta and not force_download and cond:
        log(
            "aobDl:",
            "The target file already exists. Please set --force in order to forcibly overwrite it."
        )
        exit(0)
    elif only_add_meta and not os.path.isfile(s.file_name()):
        # Note: We are always strict in terms of metadata
        log(
            "aobDl:",
            "The target file doesn't exist and --no-download is set. Please unset it in order to download."
        )
        exit(1)

    data = parse_html(f"https://www.bachvereniging.nl/en/bwv/bwv-{bwv}{modifier}/", classifiers)
    if isinstance(data, Ok):
        data = data.ok()
        if data.check_props(True) == TriState.FALSE:
            log("aobDl:", "Couldn't find all properties", True)
            exit(1)

        do_work(
            data.yt_id, data.title,
            data.album, data.genre, data.year, data.performer, data.conductor,
            data.track,
            data.movements, data.orig_lyrics.strip(), data.trans_lyrics.strip(),
            s, dir_list, lock
        )
    else:
        try:
            data.error().raise_for_status()
        except HTTPError as err:
            for arg in err.args: log("aobDl:", str(arg), True)
            log("aobDl:", "Request failed", True)
        else:
            log("aobDl:", "Request failed or couldn't find video", True)
        finally:
            exit(1)
