from dataclasses import dataclass
from typing import Optional
import platform

@dataclass
class FileFormat:
    """Format to convert the file to, after downloading."""

    extension: str
    acodec: str
    vcodec: Optional[str] = None

    def uses_video(self) -> bool:
        return self.vcodec is not None

    def map_for_system(self) -> 'FileFormat':
        return self

WAV = FileFormat("wav", "wav")
FLAC = FileFormat("flac", "flac")
MP3 = FileFormat("mp3", "mp3")
M4A = FileFormat("m4a", "aac")
MKA = FileFormat("mka", "opus")
OGGVORBIS = FileFormat("ogg", "vorbis")
OGGOPUS = FileFormat("ogg", "opus")
OPUS = FileFormat("opus", "opus")

MP4 = FileFormat("mp4", "aac", "libx265")
MKV = FileFormat("mkv", "opus", "av1")
WEBM = FileFormat("webm", "opus", "av1")
MOV = FileFormat("mov", "aac", "libx265")

COMMON_FORMATS: list[FileFormat] = [WAV, FLAC, MP3, M4A, MKA, OGGVORBIS, OGGOPUS, OPUS, MP4, MKV, WEBM, MOV]

SYSTEM_MAPPING: dict[str, dict[str, str]] = {
    # macOS
    "Darwin": {
        "libx265": "hevc_videotoolbox",
        "av1": "librav1e",
    },

    # Universally
    "*": {
        "opus": "libopus",
        "vorbis": "libvorbis",
    },
}

def get_mapping(key: str) -> str:
    system = SYSTEM_MAPPING.get(platform.system(), {})
    value = system.get(key, SYSTEM_MAPPING["*"].get(key, key))
    return value
