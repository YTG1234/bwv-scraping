from .all        import search
from .bwv        import play_or_dl
from .dl         import download
from .utils      import info_logging, log

from sys         import argv

def _e_main():
    if not info_logging(argv): exit()

    ops = {
        "all": search,
        "bwv": play_or_dl,
        "dl": download,
    }

    if len(argv) < 2 or argv[1] not in ops.keys():
        log("__main__:", "You must specify one of: " + ", ".join(ops.keys()) + ".", True)
        exit(1)

    ops[argv[1]](argv[1:])

def _e_aobDl():
    if not info_logging(argv): exit()
    download(argv)

def _e_aobAll():
    if not info_logging(argv): exit()
    search(argv)

def _e_bwv():
    if not info_logging(argv): exit()
    play_or_dl(argv)
