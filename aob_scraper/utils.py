from dataclasses import dataclass
from pkg_resources import resource_string

from enum      import Enum
from sys       import stderr
from typing    import Generic, Iterable, Optional, Sized, TypeVar, Callable, Union
from os        import system, getenv, environ
from importlib import metadata
from .formats  import FileFormat, COMMON_FORMATS, get_mapping

import shutil, os, argparse

def is_empty(sized: Optional[Sized]) -> bool:
    """Return whether a Sized object is empty.

    Arguments:
    sized -- The Sized instance"""
    return sized is not None and len(sized) < 1

def not_empty(obj: Optional[Sized]) -> bool:
    """Return whether a Sized object is not empty.

    Arguments:
    sized -- The Sized instance"""
    return (obj is not None) and (not is_empty(obj))

def none_empty(obj: Optional[Sized]) -> bool:
    return (obj is None) or is_empty(obj)

def log(grey: str, reset: str, useStderr: bool = False) -> None:
    """Prints some text in grey and some in white, seperated by a space.

    Arguments:
    grey -- The text to print in grey
    reset -- The text to print in white
    useStderr -- Whether to print to stderr instead of stdout"""

    if useStderr: return print("\x1b[90m" + grey + "\x1b[0m " + reset, file=stderr)
    return print("\x1b[90m" + grey + "\x1b[0m " + reset)

def system2(grey: str, command: str) -> int:
    """Log and run a system command.

    Arguments:
    grey -- Passed over to `log`
    command -- The command to run, calling `os.system`"""
    log(grey, "$ " + command)
    return system(command)

def is_in_env(var: str) -> bool:
    """Returns True if an environment variable's value is non-empty.

    var -- The environment variable's name"""

    if var not in environ: return False
    return not_empty(getenv(var, ""))

def get_env(var: str, default: str) -> str:
    """os.getenv but it actually works.

    var -- The environment variable name to get
    default -- The value to return if none found"""

    if is_in_env(var): return getenv(var, default)
    else: return default

T = TypeVar('T')
def first(test: Callable[[T], bool], it: Iterable[T]) -> Optional[T]:
    """Returns the first item in an iterable to match a test, and None if none match.

    Arguments:
    test -- The test to run on each element
    it -- The iterable to match on"""

    return next(filter(test, it), None)

def info_logging(argv: Iterable[str]) -> bool:
    """Prints some essential information to the console using print(),
    and returns whether the program should continue running.

    Arguments:
    argv -- The arguments passed to the program (sys.argv)"""

    print("This is aob-scraper, version " + metadata.version("aob_scraper") + ".")
    if "--license" in argv:
        print()
        print(resource_string(__name__, "LICENSE.txt").decode("utf-8"))
        return False
    print("aob-scraper is licensed under the MIT license. Pass a '--license' argument for more information.")

    return True

def cmdl_prog_installed(name: str) -> bool:
    """Checks if a program is available in PATH.

    Arguments:
    name -- The name of the program"""

    return shutil.which(name) != None

class TriState(Enum):
    FALSE = -1
    NEUTRAL = 0
    TRUE = 1

V = TypeVar("V", covariant=True)
E = TypeVar("E", covariant=True)

class Ok(Generic[V]):
    _value: V

    def __init__(self, value: V):
        self._value = value

    def ok(self) -> V:
        return self._value

    def error(self):
        return None

class Error(Generic[E]):
    _value: E

    def __init__(self, value: E):
        self._value = value

    def ok(self):
        return None

    def error(self) -> E:
        return self._value
@dataclass
class State:
    """Represents everything vital to the execution of download, in one object."""

    # Catalogue number
    bwv: int

    # Album fields (see classifier_dict)
    classifiers: list[str]

    # Part after the number, if any
    modifier: str

    # Converted file format
    file_format: FileFormat

    # Amount of semitones to transpose by
    raise_by: float

    # Set to True if we're not downloading anything off of YT
    only_add_meta: bool

    # Part to cut from the start of the video
    cut_seconds: float

    def ext(self) -> str:
        """The file extension of the converted file."""

        return self.file_format.extension

    def should_use_video(self) -> bool:
        """Returns False if the file does not contain video."""

        return self.file_format.uses_video()

    def file_name(self) -> str:
        """Returns the destination file name based on the state."""

        return ("BWV_%04d%s" % (self.bwv, self.modifier)) + (f".r.{self.raise_by}." if self.should_raise() else ".") + self.ext()

    def should_raise(self) -> bool:
        return self.raise_by > 0

def get_file_name(state: State) -> Optional[str]:
    for f in COMMON_FORMATS:
        s = State(state.bwv, state.classifiers, state.modifier, f, state.raise_by, state.only_add_meta, state.cut_seconds)
        fname = s.file_name()
        if os.path.exists(fname): return fname
    return None

def file_exists_anyf(state: State) -> Optional[str]:
    if os.path.exists(state.file_name()): return state.file_name()
    if get_file_name(state): return get_file_name(state)
    return None

def download_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--classifiers", nargs="+", help="Sets the album metadata field", default=["series", "genre"])
    parser.add_argument("--format", help="The resulting file extension (some extensions have default codecs)", dest="extension", default="ogg")
    parser.add_argument("--acodec", help="Audio codec of the resulting file", default="_default")
    parser.add_argument("--vcodec", help="Video codec of the resulting file (if applicable)", default="_default")
    parser.add_argument("--cut", help="Seconds from the beginning to remove", type=float, default=-1.0)
    parser.add_argument("--raise", dest="raises", help="Raise by semitones", type=float, default=0)
    downloading = parser.add_mutually_exclusive_group()
    downloading.add_argument("--no-download", help="Only ever add metadata to the files", action="store_true")
    downloading.add_argument("--force", help="Overwrite existing files", action="store_true")
    return parser

def play_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--player", help="The player that should be used to play", choices=["mpv", "ffplay", "aplay", "afplay"], default="mpv")
    parser.add_argument("--gui", help="Force the player to open up a GUI for audio-only files", action="store_true")
    parser.add_argument("--fullscreen", help="Force the player to open up in full-screen mode", action="store_true")
    return parser

def env_args() -> list[str]:
    env_args = get_env("AOB_ARGS", "").split(" ")
    if len(env_args) == 1 and is_empty(env_args[0]): return []
    return env_args

def audio_cmdline(s: State, acodec: str) -> str:
    if is_empty(s.file_format.acodec): return ""
    if s.file_format.acodec == acodec: return "-c:a copy"
    return "-c:a '" + get_mapping(s.file_format.acodec) + "'"

def video_cmdline(s: State, vcodec: str) -> str:
    if not s.should_use_video(): return "-vn"
    if is_empty(s.file_format.vcodec): return ""
    if s.file_format.vcodec == vcodec: return "-c:v copy"
    return "-c:v '" + get_mapping(str(s.file_format.vcodec)) + "'"

"""
A `Result` type may either be `Ok` or `Error`.
"""
Result=Union[Ok[V], Error[E]]
